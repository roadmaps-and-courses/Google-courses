import random

def firstGame(bet):
  return bet + (1 if (random.uniform(0,1) <= 0.49) else -1)

def secondGame(bet):
  prob = 0.09 if (bet % 3 == 0) else 0.74
  return bet + (1 if (random.uniform(0,1) <= prob) else -1)

def main():
  firstBet = 1000;
  secondBet = 1000;
  thirdBet = 1000;
  for i in range(1000):
    firstBet = firstGame(firstBet)
    if firstBet == 0 :
      break
  for i in range(1000):
    secondBet = secondGame(secondBet)
    if secondBet == 0 :
      break
  for i in range(1000000):
    thirdBet = firstGame(thirdBet) if (random.uniform(0,1) <= 0.50) else secondGame(thirdBet)
    if thirdBet == 0 :
      break
  print("first bet: " + str(firstBet))
  print("second bet: " + str(secondBet))
  print("third bet: " + str(thirdBet))



if __name__ == "__main__":
  main()