# Google Courses

## Virtual env management

```bash
# Create a virtual env to run the project
python -m venv ./.venv
# Enable the virtual env
# Documentation on: https://docs.python.org/es/3/library/venv.html
# On Unix
source ./.venv/bin/activate 
# On Powershell
.\.venv\Scripts\activate
# Install the requirements
pip install -r requirements.txt
# Run the notebook
jupyter notebook
# Disable the virtual env
deactivate
```